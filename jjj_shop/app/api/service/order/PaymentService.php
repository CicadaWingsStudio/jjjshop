<?php

namespace app\api\service\order;
use app\common\library\easywechat\AppWx;
use app\common\library\easywechat\WxPay;
use app\common\enum\order\OrderTypeEnum;

class PaymentService
{
    /**
     * 构建微信支付
     */
    public static function wechat(
        $user,
        $orderNo,
        $payPrice,
        $orderType = OrderTypeEnum::MASTER,
        $pay_source
    )
    {
        $app = AppWx::getWxPayApp($user['app_id']);
        $open_id = $user['open_id'];

        $WxPay = new WxPay($app);
        return  $WxPay->unifiedorder($orderNo, $open_id, $payPrice, $orderType, $pay_source);
    }

}