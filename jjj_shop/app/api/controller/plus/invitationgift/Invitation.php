<?php

namespace app\api\controller\plus\invitationgift;


use app\api\controller\Controller;
use app\api\model\plus\invitationgift\Invitation as InvitationModel;

/**
 * 邀请有礼控制器
 */
class Invitation extends Controller
{
    private $user;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        $this->user = $this->getUser();   // 用户信息
    }

    /**
     * 获取数据
     */
    public function getDatas($invitation_gift_id)
    {
        $model = new InvitationModel();
        $data = $model->getDatas($invitation_gift_id, $this->user['user_id']);
        return $this->renderSuccess('', compact('data'));
    }
}