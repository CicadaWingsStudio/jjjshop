<?php

namespace app\api\controller\user;

use app\api\controller\Controller;
use app\api\model\user\UserOpen as UserOpenModel;
use app\common\model\user\Sms as SmsModel;

/**
 * app用户管理
 */
class Useropen extends Controller
{
    /**
     * 手机号码登录
     */
    public function phonelogin()
    {
        $data = $this->request->post();
        $model = new UserOpenModel;
        $user_id = $model->phoneLogin($data);
        if ($user_id) {
            return $this->renderSuccess('', [
                'user_id' => $user_id,
                'token' => $model->getToken()
            ]);
        }
        return $this->renderError($model->getError() ?: '登录失败');
    }

    /**
     * 短信登录
     */
    public function smslogin()
    {
        $data = $this->request->post();
        $model = new UserOpenModel;
        $user_id = $model->smslogin($data);
        if ($user_id) {
            return $this->renderSuccess('', [
                'user_id' => $user_id,
                'token' => $model->getToken()
            ]);
        }
        return $this->renderError($model->getError() ?: '登录失败');
    }

    /**
     * 发送短信
     */
    public function sendCode($mobile, $type)
    {
        $model = new SmsModel();
        if ($model->send($mobile, $type)) {
            return $this->renderSuccess();
        }
        return $this->renderError($model->getError() ?: '发送失败');
    }
}
