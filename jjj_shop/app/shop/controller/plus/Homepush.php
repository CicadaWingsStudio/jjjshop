<?php

namespace app\shop\controller\plus;

use app\common\enum\settings\SettingEnum;
use app\shop\controller\Controller;
use app\shop\model\settings\Setting as SettingModel;
/**
 * 首页推送控制器
 */
class Homepush extends Controller
{
    /**
     *首页推送配置
     */
    public function index()
    {
        if($this->request->isGet()){
            $vars['values'] = SettingModel::getItem(SettingEnum::HOMEPUSH);
            return $this->renderSuccess('', compact('vars'));
        }

        $model = new SettingModel;
        if ($model->edit(SettingEnum::HOMEPUSH, $this->postData())) {
            return $this->renderSuccess('操作成功');
        }
        return $this->renderError($model->getError() ?:'操作失败');
    }

}